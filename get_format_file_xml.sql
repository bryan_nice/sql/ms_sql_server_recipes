/*************************************************************************
*
* File: get_format_file_xml.sql
* Author: Bryan J Nice
* Create date: 6 Jan 2019
* Description: Generates an XML format file that can be used by BCP to
*              load a flat file into a target table.
*
*************
* Parameters
*************
*
* @IN_TBL_NM = target table to generate an XML format file from.
* @IN_DELIMITER = flat file delimiter.
*
**********
* Returns
**********
*
* String of a XML format file that can be used by BCP to load a flat file
* into a target table.
*
**********
* History
**********
*
* Date        Author       Description
* ----------  ------------ -----------------------------------------------
* 2019-01-06: bnice         Initial version
*
***************************************************************************/

DECLARE
    @IN_TBL_NM VARCHAR(200) = '',
    @IN_DELIMITER VARCHAR(10) = ',';
SELECT
    1 AS TAG,
    NULL AS PARENT,
    'http://schemas.microsoft.com/sqlserver/2004/bulkload/format' AS [BCPFORMAT!1!xmlns],
    'http://www.w3.org/2001/XMLSchema-instance' AS [BCPFORMAT!1!xmlns:xsi],
    NULL AS [RECORD!2],
    NULL AS [FIELD!3!ID],
    NULL AS [FIELD!3!xsi:type],
    NULL AS [FIELD!3!TERMINATOR],
    NULL AS [ROW!4],
    NULL AS [COLUMN!5!SOURCE],
    NULL AS [COLUMN!5!NAME],
    NULL AS [COLUMN!5!xsi:type],
    NULL AS [COLUMN!5!PRECISION],
    NULL AS [COLUMN!5!SCALE]
UNION ALL
SELECT
    2 AS TAG,
    1 AS PARENT,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL
UNION ALL
SELECT
    3 AS TAG,
    2 AS PARENT,
    NULL,
    NULL,
    NULL,
    Field.ORDINAL_POSITION,
    'CharTerm',
    CASE
        WHEN (SELECT MAX(ORDINAL_POSITION) FROM INFORMATION_SCHEMA.COLUMNS A WHERE A.TABLE_NAME = Field.TABLE_NAME) =
            Field.ORDINAL_POSITION
        THEN '\n'
        ELSE @IN_DELIMITER
    END,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL
FROM
    INFORMATION_SCHEMA.COLUMNS AS Field
WHERE
    Field.TABLE_NAME = @IN_TBL_NM
UNION ALL
SELECT
    4 AS TAG,
    1 AS PARENT,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL
UNION ALL
SELECT
    5 AS TAG,
    4 AS PARENT,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    Field.ORDINAL_POSITION,
    Field.COLUMN_NAME,
    'SQL' + UPPER(
        CASE
            WHEN UPPER(Field.DATA_TYPE) = 'VARCHAR'
            THEN 'VARYCHAR'
            ELSE DATA_TYPE
        END
    ),
    CASE
        WHEN UPPER(DATA_TYPE) IN ('NUMERIC', 'DECIMAL')
        THEN CAST(NUMERIC_PRECISION AS VARCHAR(25))
        ELSE NULL
    END,
    CASE
        WHEN UPPER(DATA_TYPE) IN ('NUMERIC', 'DECIMAL')
        THEN CAST(NUMERIC_SCALE AS VARCHAR(25))
        ELSE NULL
    END
FROM
    INFORMATION_SCHEMA.COLUMNS AS Field
WHERE
    Field.TABLE_NAME = @IN_TBL_NM
FOR XML EXPLICIT;